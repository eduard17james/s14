function numAdd(a,b) {
	return a + b;
};

let Sum = numAdd(2, 3);
console.log("The Sum is: " + Sum);

function numSub(a,b) {
	return a - b;
};

let Sub = numSub(2, 3);
console.log("The difference is: " + Sub);

function numMul(a,b) {
	return a * b;
};

let Mul = numMul(2,3);
console.log("The Product is: " + Mul);

let product = Mul;
console.log(product);
